<?php

namespace ConverterBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ConvertType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fromCurrencyOptions = array();
        $toCurrencyOptions = array();

        if (isset($options['fromCurrency'])) {
            $fromCurrencyOptions = array('data' => $options['fromCurrency']);
        }

        if (isset($options['toCurrency'])) {
            $toCurrencyOptions = array('data' => $options['toCurrency']);
        }

        $builder
            ->add('fromCurrency', 'currency', $fromCurrencyOptions)
            ->add('toCurrency', 'currency', $toCurrencyOptions)
            ->add('amount')
            ->add('conversionResult', 'text', array('read_only' => true))
            ->add('convert', 'submit')
            ->setAction('convert_currency')->setMethod('POST');
    }

    public function getName()
    {
        return 'convert';
    }
}