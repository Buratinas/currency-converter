<?php

namespace ConverterBundle\Services;

class DefaultConverter
{
    /**
     * @var Configuration
     */
    private $configurationService;

    /**
     * @param Configuration $configurationService
     */
    public function __construct(Configuration $configurationService)
    {
        $this->setConfigurationService($configurationService);
    }

    /**
     * @return Configuration
     */
    public function getConfigurationService()
    {
        return $this->configurationService;
    }

    /**
     * @param Configuration $configurationService
     */
    public function setConfigurationService($configurationService)
    {
        $this->configurationService = $configurationService;
    }

    /**
     * Goes through exchanges and tries to collect currency information
     */
    public function getExchangeInformation()
    {
        $data = array();

        $exchanges = $this->getConfigurationService()->getExchangeStorageService()->getExchangeParsers();
        if (!is_array($exchanges)) {
            return $data;
        }

        /** @var AbstractExchangeParser $exchange */
        foreach ($exchanges as $exchange) {
            $data = array_merge($data, $exchange->parseExchanges());
        }

        return $data;
    }

    /**
     * Converts currency using default conversion type
     *
     * @param string $fromCurrency Currency to convert from
     * @param string $toCurrency   Currency to convert to
     * @param float  $amount       Amount to convert
     *
     * @return float
     */
    public function convertCurrency($fromCurrency, $toCurrency, $amount)
    {
        $date = new \DateTime(date('Y/m/d'));
        $rounder = $this->getConfigurationService()->getRounderService();
        $precision = $this->getConfigurationService()->getPrecisionService();

        $convertedValue = $this->convertCurrencyOnDate($fromCurrency, $toCurrency, $amount, $date);
        $roundedValue = $rounder->applyTo($convertedValue);
        $formattedValue = $precision->addPrecision($roundedValue);

        return $formattedValue;
    }

    /**
     * Converts currency using specific date
     *
     * @param string    $fromCurrency Currency to convert from
     * @param string    $toCurrency   Currency to convert to
     * @param float     $amount       Amount to convert
     * @param \DateTime $date         Date to use currencies from
     *
     * @return float
     *
     * @throws \Exception
     */
    public function convertCurrencyOnDate($fromCurrency, $toCurrency, $amount, $date)
    {
        $configuration = $this->getConfigurationService();
        $exchangeStorage = $configuration->getExchangeStorageService();
        $currencies = $exchangeStorage->getCurrencies(array($fromCurrency, $toCurrency), $date);
        $convertedValue = $currencies['vT'] / $currencies['vF'] * $amount;
        return $convertedValue;
    }
}