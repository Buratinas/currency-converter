<?php

namespace ConverterBundle\Services;

abstract class AbstractExchangeParser
{
    /**
     * @var array
     */
    private $exchangeUrls = array();

    /**
     * Parses existing exchanges, if any are set
     */
    public function parseExchanges()
    {
        $data = array();
        $exchangeUrls = $this->getExchangeUrls();
        if (!is_array($exchangeUrls)) {
            return $data;
        }

        foreach ($exchangeUrls as $baselineCurrency => $urls)
        {
            foreach ($urls as $url) {
                $data[] = $this->parseExchange($url, $baselineCurrency);
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getExchangeUrls()
    {
        return $this->exchangeUrls;
    }

    /**
     * @param array $exchangeUrls Exchange urls
     */
    public function setExchangeUrls($exchangeUrls)
    {
        $this->exchangeUrls = $exchangeUrls;
    }

    /**
     * Adds a single exchange url to existing urls
     *
     * @param string $exchangeUrl      Exchange url
     * @param string $baselineCurrency Baseline currency
     */
    public function addExchangeUrl($exchangeUrl, $baselineCurrency)
    {
        $this->exchangeUrls[$baselineCurrency][] = $exchangeUrl;
    }

    /**
     * Parse the exchange and return relevant information from it
     * Requires baseline currency to have a reference of conversion
     *
     * @param string $url              Exchange url
     * @param string $baselineCurrency Baseline currency value
     *
     * @return \ConverterBundle\Entity\Exchange
     */
    abstract public function parseExchange($url, $baselineCurrency);

    /**
     * Returns name of exchange parser for easier understanding
     *
     * @return string
     */
    abstract public function getName();
}