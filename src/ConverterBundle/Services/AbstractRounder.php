<?php
/**
 * Created by PhpStorm.
 * User: tadas
 * Date: 10/5/15
 * Time: 11:22 PM
 */

namespace ConverterBundle\Services;


abstract class AbstractRounder
{
    /**
     * Rounds value based on defined rules
     *
     * @param int $value Value to be rounded
     */
    abstract public function applyTo($value);
}