<?php
/**
 * Created by PhpStorm.
 * User: tadas
 * Date: 10/5/15
 * Time: 11:36 PM
 */

namespace ConverterBundle\Services;


class MoneyFormat extends AbstractPrecision
{
    private $format = '%i';

    public function __construct($format)
    {
        if (!empty($format)) {
            $this->format = $format;
        }
    }

    /**
     * @inheritdoc
     */
    public function addPrecision($value)
    {
        return money_format($this->format, $value);
    }
}