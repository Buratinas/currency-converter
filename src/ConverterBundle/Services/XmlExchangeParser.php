<?php

namespace ConverterBundle\Services;


use ConverterBundle\Entity\Currency;
use ConverterBundle\Entity\Exchange;

class XmlExchangeParser extends AbstractExchangeParser
{
    private $parseFunction = null;

    /**
     * Constructor
     * Builds a parse function which is used to parse xml from url
     * It is done this way to make mocking easier
     * Might not be the best way, but I wanted to try it :)
     */
    public function __construct()
    {
        $parserFunction = function($url)
        {
            return simplexml_load_file($url);
        };
        $this->setParseFunction($parserFunction);
    }

    /**
     * @return null
     */
    public function getParseFunction()
    {
        return $this->parseFunction;
    }

    /**
     * @param null $parseFunction
     */
    public function setParseFunction($parseFunction)
    {
        $this->parseFunction = $parseFunction;
    }

    /**
     * @inheritDoc
     */
    public function parseExchange($url, $baselineCurrency)
    {
        libxml_clear_errors();
        libxml_use_internal_errors(true);
        $xml = call_user_func($this->getParseFunction(), $url);
        $errors = libxml_get_errors();
        if (!empty($errors)) {
            return '';
        }

        $exchange = $this->parseXml($xml);

        if ($exchange instanceof Exchange) {
            $exchange->setUrl($url);
            $exchange->setBaselineCurrency($baselineCurrency);
            $baseCurrency = new Currency();
            $baseCurrency->setValue(1);
            $baseCurrency->setCurrency($baselineCurrency);
            $baseCurrency->setExchange($exchange);
            $exchange->addCurrency($baseCurrency);
        }
        return $exchange;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'XmlExchangeParser';
    }

    /**
     * Parses XML for existing currency codes using format
     * XML->Cube->Cube->Cube
     * Returns parsed Xml with time and currencies as \StdClass object
     *
     * @param \SimpleXMLElement $xml Xml element containing currencies
     *
     * @return Exchange
     */
    protected function parseXml($xml)
    {
        $currencies = array();
        if (!isset($xml->Cube->Cube->Cube)) {
            return null;
        }

        $exchange = new Exchange();
        foreach ($xml->Cube->Cube->Cube as $rate) {
            $currency = new Currency();
            $currency->setCurrency((string)$rate['currency']);
            $currency->setValue((string)$rate['rate']);
            $currency->setExchange($exchange);
            $currencies[] = $currency;
        }

        $exchange->setCurrencies($currencies);
        $date = (string)$xml->Cube->Cube['time'];
        $dateTime = new \DateTime(date('Y/m/d', strtotime($date)));
        $exchange->setDateReceived($dateTime);
        return $exchange;
    }

}