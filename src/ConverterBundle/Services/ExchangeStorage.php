<?php

namespace ConverterBundle\Services;

use ConverterBundle\Entity\Exchange;
use ConverterBundle\Repository\ExchangeRepository;
use Doctrine\ORM\EntityManager;

class ExchangeStorage
{
    /** @var EntityManager  */
    private $em;

    /** @var AbstractExchangeParser */
    private $exchangeParsers = array();

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getExchangeParsers()
    {
        return $this->exchangeParsers;
    }

    /**
     * @param array $exchangeParsers
     */
    public function setExchangeParsers($exchangeParsers)
    {
        $this->exchangeParsers = $exchangeParsers;
    }

    /**
     * Adds additional exchange parser
     *
     * @param AbstractExchangeParser $exchangeParser
     */
    public function addExchangeParserService(AbstractExchangeParser $exchangeParser)
    {
        $this->exchangeParsers[] = $exchangeParser;
    }

    /**
     * Returns both currencies by parsing existing exchanges
     * Exchange information is persisted to database
     *
     * @param array $currencies Currency array (from, to)
     *
     * @return array
     */
    public function getCurrenciesFromExchanges(array $currencies)
    {
        $exchangeParsers = $this->getExchangeParsers();
        /** @var AbstractExchangeParser $exchangeParser */
        foreach ($exchangeParsers as $exchangeParser) {
            $exchangeInformation = $exchangeParser->parseExchanges();
            $this->storeExchangeInformation($exchangeInformation);
        }
        list($from, $to) = $currencies;
        return $this->getExchangeRepository()->findOneByBothCurrencies($from, $to);
    }

    /**
     * Parses exchanges and stores information to database
     *
     * @param array $exchangeInformation Exchange information array
     */
    protected function storeExchangeInformation($exchangeInformation)
    {
        /** @var Exchange $singleExchangeInformation */
        foreach ($exchangeInformation as $singleExchangeInformation) {
            if ($singleExchangeInformation instanceof Exchange) {
                $this->em->persist($singleExchangeInformation);
            }
        }
        $this->em->flush();
    }

    /**
     * Returns both currencies using database and exchange information
     * Exchanges are parsed if $date is not set and no results are found in database
     *
     * @param array          $currencies Currency array (from, to)
     * @param null|\DateTime $date       DDate which conversion rates to use
     * @return array
     *
     * @throws \Exception
     */
    public function getCurrencies(array $currencies, $date = null)
    {
        list($from, $to) = $currencies;
        $currencyInformation = $this->getExchangeRepository()->findOneByBothCurrencies($from, $to, $date);
        $now = new \DateTime(date('Y/m/d'));
        if (is_null($currencyInformation) && (is_null($date) || $date->format('Y/m/d') == $now->format('Y/m/d'))) {
            $currencyInformation = $this->getCurrenciesFromExchanges($currencies);
        }

        if (is_null($currencyInformation)) {
            throw new \Exception(sprintf('Could not find currencies in database'));
        }

        return $currencyInformation;
    }

    /**
     * Returns Currency repository
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getCurrencyRepository()
    {
        return $this->em->getRepository('ConverterBundle:Currency');
    }

    /**
     * Returns Exchange repository
     *
     * @return ExchangeRepository
     */
    protected function getExchangeRepository()
    {
        return $this->em->getRepository('ConverterBundle:Exchange');
    }

}