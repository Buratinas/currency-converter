<?php

namespace ConverterBundle\Services;


class CustomizableFloorRounder extends AbstractRounder
{
    /**
     * @var int
     */
    private $roundingDigits;

    public function __construct($roundingDigits = 0)
    {
        $this->roundingDigits = $roundingDigits;
    }

    /**
     * @inheritdoc
     */
    public function applyTo($value)
    {
        $digits = (int)$this->roundingDigits;
        $power = pow(10, $digits);
        $flooredValue = floor($value * $power) / $power;

        return $flooredValue;
    }
}