<?php
/**
 * Created by PhpStorm.
 * User: tadas
 * Date: 10/5/15
 * Time: 11:22 PM
 */

namespace ConverterBundle\Services;


class FloorRounder extends AbstractRounder
{
    /**
     * @inheritdoc
     */
    public function applyTo($value)
    {
        return floor($value);
    }
}