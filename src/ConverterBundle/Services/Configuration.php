<?php

namespace ConverterBundle\Services;


class Configuration
{
    private $precisionService;

    private $rounderService;

    private $exchangeStorageService;

    public function __construct(
        AbstractRounder $rounderService,
        AbstractPrecision $precisionService,
        ExchangeStorage $exchangeService
    )
    {
        $this->setPrecisionService($precisionService);
        $this->setRounderService($rounderService);
        $this->setExchangeStorageService($exchangeService);
    }

    /**
     * @return AbstractPrecision
     */
    public function getPrecisionService()
    {
        return $this->precisionService;
    }

    /**
     * @param AbstractPrecision $precisionService
     */
    public function setPrecisionService($precisionService)
    {
        $this->precisionService = $precisionService;
    }

    /**
     * @return AbstractRounder
     */
    public function getRounderService()
    {
        return $this->rounderService;
    }

    /**
     * @param AbstractRounder $rounderService
     */
    public function setRounderService($rounderService)
    {
        $this->rounderService = $rounderService;
    }

    /**
     * @return ExchangeStorage
     */
    public function getExchangeStorageService()
    {
        return $this->exchangeStorageService;
    }

    /**
     * @param ExchangeStorage $exchangeStorage
     */
    public function setExchangeStorageService($exchangeStorage)
    {
        $this->exchangeStorageService = $exchangeStorage;
    }

}