<?php
/**
 * Created by PhpStorm.
 * User: tadas
 * Date: 10/5/15
 * Time: 11:34 PM
 */

namespace ConverterBundle\Services;


abstract class AbstractPrecision
{
    /**
     * Adds precision to given number
     *
     * @param int $value Number value
     *
     * @return mixed
     */
    abstract public function addPrecision($value);
}