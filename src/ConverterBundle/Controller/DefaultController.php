<?php

namespace ConverterBundle\Controller;

use ConverterBundle\Entity\ConversionHistory;
use ConverterBundle\Form\Type\ConvertType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/converter/{fromCurrency}/{toCurrency}", defaults={"toCurrency"="EUR", "toCurrency"="LTL"})
     * @Template()
     */
    public function indexAction($fromCurrency = 'EUR', $toCurrency = 'LTL')
    {
        $conversionHistory = new ConversionHistory();
        $conversionHistory->setFromCurrency($fromCurrency);
        $conversionHistory->setToCurrency($toCurrency);
        $form = $this->createForm(new ConvertType(), $conversionHistory);

        /** @var Request $request */
        $request = $this->get('request');
        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $conversionHistory->setConversionResult($this->doConversion($conversionHistory));
            unset($form);
            $form = $this->createForm(new ConvertType(), $conversionHistory);
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * Attempts to run a conversion
     *
     * @param ConversionHistory $conversionHistory Conversion history object
     *
     * @return float
     */
    public function doConversion(ConversionHistory $conversionHistory)
    {
        $converter = $this->get('converter.default_converter');
        return $converter->convertCurrency(
            $conversionHistory->getFromCurrency(),
            $conversionHistory->getToCurrency(),
            $conversionHistory->getAmount()
        );
    }

    /**
     * Converts using ajax request
     *
     * @Route("/convert_currency")
     * @Method("POST")
     */
    public function convertAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Can only be called with Ajax!'), 400);
        }

        $conversionHistory = new ConversionHistory();
        $form = $this->createForm(new ConvertType(), $conversionHistory);
        $form->handleRequest($request);

        $errors = array();
        $value = '';
        try {
            $value = $this->doConversion($conversionHistory);
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }
        return new JsonResponse(array('errors' => $errors, 'value' => $value), 200);
    }
}
