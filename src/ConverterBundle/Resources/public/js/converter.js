var $fromCurrency = $('#convert_fromCurrency');
var $toCurrency = $('#convert_toCurrency');
var $amount = $('#convert_amount');
var $result = $('#convert_conversionResult');

valueChange = function() {
    var $form = $(this).closest('form');
    var data = {};
    data[$fromCurrency.attr('name')] = $fromCurrency.val();
    data[$toCurrency.attr('name')] = $toCurrency.val();
    data[$amount.attr('name')] = $amount.val();
    $.ajax({
        url : $form.attr('action'),
        type : $form.attr('method'),
        data : data,
        success : function (html) {
            $result.val(html.value);
        }
    });
};
$fromCurrency.change(valueChange);
$toCurrency.change(valueChange);
$amount.change(valueChange);
