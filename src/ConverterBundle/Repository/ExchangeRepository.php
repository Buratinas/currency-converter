<?php

namespace ConverterBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ExchangeRepository extends EntityRepository
{
    /**
     * Tries to get both currencies from database on specific date on newest
     *
     * @param string         $from From currency
     * @param string         $to   To currency
     * @param null|\DateTime $date Date which conversion rates to use
     *
     * @return array
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByBothCurrencies($from, $to, $date = null)
    {
        $query = $this->createQueryBuilder('e');
        $query->select('e, cFrom.currency as cF, cFrom.value as vF, cTo.currency as cT, cTo.value as vT')
            ->leftJoin('e.currencies', 'cFrom', 'WITH', 'cFrom.currency = :curFrom')
            ->leftJoin('e.currencies', 'cTo', 'WITH', 'cTo.currency = :curTo')
            ->where('cFrom.currency IS NOT NULL')
            ->andWhere('cTo.currency IS NOT NULL')
            ->orderBy('e.dateReceived', 'DESC')
            ->setParameters(array('curFrom' => $from, 'curTo' => $to))
            ->setMaxResults(1);
        if (!is_null($date)) {
            $query->andWhere('e.dateReceived = :date')
                ->setParameter('date', $date);
        }

        return $query->getQuery()->getOneOrNullResult();
    }
}