<?php

namespace ConverterBundle\Tests\Unit\Services;

use ConverterBundle\Services\Configuration;
use ConverterBundle\Services\CustomizableFloorRounder;
use ConverterBundle\Services\DefaultConverter;
use ConverterBundle\Services\ExchangeStorage;
use ConverterBundle\Services\MoneyFormat;
use ConverterBundle\Services\XmlExchangeParser;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DefaultConverterTest extends KernelTestCase
{
    const NO_EXCHANGE_URL = 1;

    const TWO_EXCHANGES = 2;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testGetExchangeInformationSingleExchange()
    {
        $expectedExchangeInformation = array($this->getExchangeInformation());

        $configuration = $this->getConfiguration();
        $converter = new DefaultConverter($configuration);
        $this->assertEquals($expectedExchangeInformation, $converter->getExchangeInformation());
    }

    public function testGetExchangeInformationNoExchange()
    {
        $configuration = $this->getConfiguration(static::NO_EXCHANGE_URL);
        $converter = new DefaultConverter($configuration);
        $this->assertEquals(array(), $converter->getExchangeInformation());
    }

    public function testGetExchangeInformationTwoExchanges()
    {
        $expectedExchangeInformation = array($this->getExchangeInformation(), $this->getExchangeInformation());
        $configuration = $this->getConfiguration(static::TWO_EXCHANGES);
        $converter = new DefaultConverter($configuration);
        $this->assertEquals($expectedExchangeInformation, $converter->getExchangeInformation());
    }

    /**
     * As rounding removes everything after the dot...
     */
    public function testConvertCurrency()
    {
        $configuration = $this->getConfiguration();
        $converter = new DefaultConverter($configuration);

        $this->assertEquals(11, $converter->convertCurrency('EUR', 'USD', 10));
    }

    private function getConfiguration($option = null)
    {
        $rounder = new CustomizableFloorRounder();
        $precision = new MoneyFormat('');
        $exchange = $this->getMock('ConverterBundle\Services\XmlExchangeParser', array('parseExchange'));
        $exchange->expects($this->any())->method('parseExchange')->willReturn($this->getExchangeInformation());
        if ($option != static::NO_EXCHANGE_URL) {
            $exchange->addExchangeUrl('test', 'EUR');
        }
        $exchangeStorage = new ExchangeStorage($this->em);
        $exchangeStorage->addExchangeParserService($exchange);

        $configuration = new Configuration($rounder, $precision, $exchangeStorage);
        if ($option === static::TWO_EXCHANGES) {
            $secondExchange = clone $exchange;
            $configuration->getExchangeStorageService()->addExchangeParserService($secondExchange);
        }

        return $configuration;
    }

    private function getExchangeInformation()
    {
        $expectedObject = new \StdClass();
        $expectedObject->time = '2015-10-06';
        $expectedObject->url = 'test';
        $expectedObject->currencies = array('USD' => '1.1224');

        return $expectedObject;
    }
}
