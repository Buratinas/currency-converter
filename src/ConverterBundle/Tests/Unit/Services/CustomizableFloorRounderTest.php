<?php
/**
 * Created by PhpStorm.
 * User: tadas
 * Date: 10/6/15
 * Time: 9:16 PM
 */

namespace ConverterBundle\Tests\Services;


use ConverterBundle\Services\CustomizableFloorRounder;

class CustomizableFloorRounderTest extends \PHPUnit_Framework_TestCase
{
    public function testApplyToDefaultConstructor()
    {
        $rounder = new CustomizableFloorRounder();
        $this->assertEquals(11, $rounder->applyTo(11.999));
    }

    /**
     * @return array
     */
    public function applyToDataProvider()
    {
        return array(
            array(159.44, 2, 159.44),
            array(0.4409, 2, 0.44),
            array(5545.1234, 3, 5545.123),
            array(123.456, -2, 100), // this case shows that you can move rounding either way
        );
    }

    /**
     * @dataProvider applyToDataProvider
     */
    public function testApplyTo($value, $digits, $expectedValue)
    {
        $rounder = new CustomizableFloorRounder($digits);
        $this->assertEquals($expectedValue, $rounder->applyTo($value));
    }
}
