<?php

namespace ConverterBundle\Services;


class MoneyFormatTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function moneyFormatProvider()
    {
        return array(
            array(145.567, '%i', 145.57),
            array(145, '%i', 145.00),
            array(-145, '%i', -145.00),
            array(null, '%i', 0.00),
            array(null, null, 0.00),
            array(123.456, '%.3n', 123.456),
            array(-123.456, '%.3n', -123.456),
            array(0, '%.3n', 0),
        );
    }

    /**
     * @dataProvider moneyFormatProvider
     */
    public function testMoneyFormat($value, $format, $expectedValue)
    {
        $moneyFormat = new MoneyFormat($format);
        $this->assertEquals($expectedValue, $moneyFormat->addPrecision($value));
    }
}
