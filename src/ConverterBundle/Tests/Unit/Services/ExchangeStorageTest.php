<?php

namespace ConverterBundle\Tests\Unit\Services;


use ConverterBundle\Services\ExchangeStorage;
use ConverterBundle\Services\XmlExchangeParser;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExchangeStorageTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testAddAdditionalExchanges()
    {
        $exchange = new XmlExchangeParser();
        $secondExchange = new XmlExchangeParser();

        $storage = new ExchangeStorage($this->em);
        $this->assertEquals(array(), $storage->getExchangeParsers());

        $storage->addExchangeParserService($exchange);
        $this->assertEquals(array($exchange), $storage->getExchangeParsers());

        $storage->addExchangeParserService($secondExchange);
        $this->assertEquals(array($exchange, $secondExchange), $storage->getExchangeParsers());
    }

}
