<?php

namespace ConverterBundle\Tests\Services;


use ConverterBundle\Services\FloorRounder;

class FloorRounderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function applyToDataProvider()
    {
        return array(
            array(10, 10),
            array(0, 0),
            array(0.1, 0),
            array(0.9, 0),
            array(0.49, 0),
            array(1.99999, 1),
            array(-78.99999, -79),
            array(null, 0),
        );
    }

    /**
     * @dataProvider applyToDataProvider
     */
    public function testApplyTo($value, $expectedValue)
    {
        $rounder = new FloorRounder();
        $this->assertEquals($expectedValue, $rounder->applyTo($value));
    }
}
