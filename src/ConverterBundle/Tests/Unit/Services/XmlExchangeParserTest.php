<?php
/**
 * Created by PhpStorm.
 * User: tadas
 * Date: 10/7/15
 * Time: 10:48 PM
 */

namespace ConverterBundle\Tests\Services;


use ClassesWithParents\E;
use ConverterBundle\Entity\Currency;
use ConverterBundle\Entity\Exchange;
use ConverterBundle\Services\XmlExchangeParser;

class XmlExchangeParserTest extends \PHPUnit_Framework_TestCase
{
    public function testAddExchange()
    {
        $exchange = new XmlExchangeParser();
        $this->assertEquals(array(), $exchange->getExchangeUrls());

        $exchange->addExchangeUrl('test', 'EUR');
        $this->assertEquals(array('EUR' => array('test')), $exchange->getExchangeUrls());

        $exchangeUrls = array('EUR' => array('x'), 'USD' => array('y'));
        $exchange->setExchangeUrls($exchangeUrls);
        $this->assertEquals($exchangeUrls, $exchange->getExchangeUrls());

        $exchange->addExchangeUrl('eur_test', 'EUR');
        $exchangeUrls['EUR'][] = 'eur_test';
        $this->assertEquals($exchangeUrls, $exchange->getExchangeUrls());
    }

    public function exchangeDataProvider()
    {
        $urls = array('EUR' => array('x', 'y'));
        $usdUrls = array('USD' => array('z'));
        return array(
            array(array(), array()),
            array($urls, array($this->getExpectedObject('x'), $this->getExpectedObject('y'))),
            array(
                array_merge($urls, $usdUrls),
                array(
                    $this->getExpectedObject('x'),
                    $this->getExpectedObject('y'),
                    $this->getExpectedObject('z', 'USD')
                )),
        );
    }

    /**
     * @dataProvider exchangeDataProvider
     */
    public function testParseExchanges($exchangeUrls, $expectedResult)
    {
        $exchange = $this->getMock('ConverterBundle\Services\XmlExchangeParser', array('parseExchange'));
        $counter = 0;
        foreach ($exchangeUrls as $baselineCurrency => $urls) {
            foreach ($urls as $url) {
                $exchange->expects($this->at($counter))
                    ->method('parseExchange')
                    ->willReturn($this->getExpectedObject($url, $baselineCurrency));
                $counter++;
            }
        }
        $exchange->setExchangeUrls($exchangeUrls);

        $this->assertEquals($expectedResult, $exchange->parseExchanges());
    }

    public function testParseExchangesInvalidValues()
    {
        $exchange = $this->getMock('ConverterBundle\Services\XmlExchangeParser', array('parseExchange'));
        $exchange->expects($this->any())->method('parseExchange')->willReturn(0);
        $exchange->setExchangeUrls(-5);

        $this->assertEquals(array(), $exchange->parseExchanges());
    }

    public function testParseExchangeInvalidXml()
    {
        $exchange = new XmlExchangeParser();

        $this->assertEquals('', $exchange->parseExchange('test', 'EUR'));
    }

    /**
     * Simulates parseFunction to avoid calling real url and parsing it
     */
    public function testParseExchangeValidXmlInvalidFormat()
    {
        $exchange = new XmlExchangeParser();
        $exchange->setParseFunction($this->getParserFunction("valid xml"));

        $this->assertNull($exchange->parseExchange('test', 'EUR'));
    }

    public function testParseExchangeValidXmlValidFormat()
    {

        $validXml = simplexml_load_string("<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <parent>
            <Cube>
                <Cube time='2015-10-06'>
                    <Cube currency='USD' rate='1.1224'/>
                </Cube>
            </Cube>
        </parent>");
        $exchange = new XmlExchangeParser();
        $exchange->setParseFunction($this->getParserFunction($validXml));

        $expectedObject = $this->getExpectedObject('test');

        $this->assertEquals($expectedObject, $exchange->parseExchange('test', $expectedObject->getBaselineCurrency()));
    }

    private function getParserFunction($returnData)
    {
        $parseFunction = function() use ($returnData)
        {
            return $returnData;
        };

        return $parseFunction;
    }

    private function getExpectedObject($url, $baselineCurrency = 'EUR')
    {
        $currency = new Currency();
        $currency->setCurrency('USD');
        $currency->setValue('1.1224');

        $expectedObject = new Exchange();
        $expectedObject->setDateReceived(new \DateTime('2015-10-06'));
        $expectedObject->setUrl($url);
        $expectedObject->setBaselineCurrency($baselineCurrency);
        $expectedObject->setCurrencies(array($currency));
        $currency->setExchange($expectedObject);

        $baselineCurrencyEntity = clone $currency;
        $baselineCurrencyEntity->setCurrency($baselineCurrency);
        $baselineCurrencyEntity->setValue(1);
        $expectedObject->addCurrency($baselineCurrencyEntity);

        return $expectedObject;
    }
}
