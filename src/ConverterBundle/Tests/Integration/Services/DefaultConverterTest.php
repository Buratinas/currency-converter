<?php

namespace ConverterBundle\Tests\Integration\Services;

use ConverterBundle\Services\Configuration;
use ConverterBundle\Services\CustomizableFloorRounder;
use ConverterBundle\Services\DefaultConverter;
use ConverterBundle\Services\ExchangeStorage;
use ConverterBundle\Services\MoneyFormat;
use ConverterBundle\Services\XmlExchangeParser;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DefaultConverterTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Using simple comparison with 10% larger number than amount as USD/EUR exchange varies and we dont expect it
     * to drop below 1.1 soon
     */
    public function testConvert()
    {
        $configuration = $this->getConfiguration();
        $converter = new DefaultConverter($configuration);

        $this->assertTrue(165 < $converter->convertCurrency('EUR', 'USD', 150));
    }

    /**
     * Returns configuration
     *
     * @return Configuration
     */
    private function getConfiguration()
    {
        $rounder = new CustomizableFloorRounder(2);
        $precision = new MoneyFormat('');
        $exchange = new XmlExchangeParser();
        $exchange->addExchangeUrl('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml', 'EUR');
        $exchangeStorage = new ExchangeStorage($this->em);
        $exchangeStorage->addExchangeParserService($exchange);

        $configuration = new Configuration($rounder, $precision, $exchangeStorage);

        return $configuration;
    }
}
