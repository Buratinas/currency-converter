<?php

namespace ConverterBundle\Tests\Services;

use ConverterBundle\Services\XmlExchangeParser;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class ExchangeParserTest
 *
 * This will do actual request so this is not a unit test!
 *
 * @package ConverterBundle\Tests\Services
 */
class ExchangeParserTest extends KernelTestCase
{
    private $exchangeUrl = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testParseExchange()
    {
        $exchangeParser = new XmlExchangeParser();
        $parsedExchangeData = $exchangeParser->parseExchange($this->exchangeUrl, 'EUR');
        $this->assertNotNull($parsedExchangeData);
        $this->assertInstanceOf('\ConverterBundle\Entity\Exchange', $parsedExchangeData);

        $this->assertInstanceOF('\DateTime', $parsedExchangeData->getDateReceived());

        $dateReceived = $parsedExchangeData->getDateReceived();
        $this->assertEquals(32, count($parsedExchangeData->getCurrencies()));

        $this->em->persist($parsedExchangeData);
        $this->em->flush($parsedExchangeData);

        $exchangeRepository = $this->em->getRepository('ConverterBundle\Entity\Exchange');
        $foundEntity = $exchangeRepository->findOneBy(array('dateReceived' => $dateReceived));
        $this->assertNotNull($foundEntity);

        $this->em->remove($foundEntity);
        $this->em->flush($foundEntity);
    }

    public function testXmlParseExchangeName()
    {
        $xmlExchangeParser = new XmlExchangeParser();
        $this->assertEquals('XmlExchangeParser', $xmlExchangeParser->getName());
    }
}
