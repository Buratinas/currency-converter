<?php

namespace ConverterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Exchange
 * @ORM\Entity(repositoryClass="ConverterBundle\Repository\ExchangeRepository")
 * @package ConverterBundle\Entity
 */
class Exchange
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="url", type="string", nullable=false)
     */
    protected $url;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_received", type="datetime")
     */
    protected $dateReceived;

    /**
     * @var string
     * @ORM\Column(name="baseline_currency", type="string", length=3)
     */
    protected $baselineCurrency;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="ConverterBundle\Entity\Currency", mappedBy="exchange", cascade={"persist", "remove"})
     */
    protected $currencies;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return \DateTime
     */
    public function getDateReceived()
    {
        return $this->dateReceived;
    }

    /**
     * @param \DateTime $dateReceived
     */
    public function setDateReceived($dateReceived)
    {
        $this->dateReceived = $dateReceived;
    }

    /**
     * @return string
     */
    public function getBaselineCurrency()
    {
        return $this->baselineCurrency;
    }

    /**
     * @param string $baselineCurrency
     */
    public function setBaselineCurrency($baselineCurrency)
    {
        $this->baselineCurrency = $baselineCurrency;
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @param array $currencies
     */
    public function setCurrencies($currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * Add a single currency to existing currencies
     *
     * @param Currency $currency Currency entity
     */
    public function addCurrency(Currency $currency)
    {
        if (!is_array($this->currencies)) {
            $this->currencies = array();
        }
           $this->currencies[] = $currency;
    }
}