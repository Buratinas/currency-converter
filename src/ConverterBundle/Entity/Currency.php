<?php

namespace ConverterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Currency
 * @ORM\Entity
 * @package ConverterBundle\Entity
 */
class Currency
{
    /**
     * @var
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(name="exchange_id", type="integer", length=11, nullable=true)
     */
    protected $exchangeId;

    /**
     * @var string
     * @ORM\Column(name="currency", length=3, nullable=false)
     */
    protected $currency;

    /**
     * @var float
     * @ORM\Column(name="value", type="float", length=10, nullable=false)
     */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="ConverterBundle\Entity\Exchange", inversedBy="currencies", cascade={"persist", "remove"})
     */
    protected $exchange;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getExchangeId()
    {
        return $this->exchangeId;
    }

    /**
     * @param int $exchangeId
     */
    public function setExchangeId($exchangeId)
    {
        $this->exchangeId = $exchangeId;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return Exchange
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @param Exchange $exchange
     */
    public function setExchange($exchange)
    {
        $this->exchange = $exchange;
    }
}