<?php

namespace ConverterBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ConversionHistory
{
    /**
     * @Assert\NotNull()
     * @Assert\Currency()
     */
    protected $fromCurrency;

    /**
     * @Assert\NotNull()
     * @Assert\Currency()
     */
    protected $toCurrency;

    /**
     * @Assert\NotBlank
     * @Assert\Range(
     *      min = 0,
     *      max = 999999999,
     *      minMessage = "Your {{ limit }} unit is too small or too wrong...(Think positive)",
     *      maxMessage = "It seems you're trying to convert {{ limit }} units, which is impressive, but a bit too much"
     * )
     */
    protected $amount;

    /**
     * @var
     */
    protected $conversionResult;

    /**
     * @return mixed
     */
    public function getFromCurrency()
    {
        return $this->fromCurrency;
    }

    /**
     * @param mixed $fromCurrency
     */
    public function setFromCurrency($fromCurrency)
    {
        $this->fromCurrency = $fromCurrency;
    }

    /**
     * @return mixed
     */
    public function getToCurrency()
    {
        return $this->toCurrency;
    }

    /**
     * @param mixed $toCurrency
     */
    public function setToCurrency($toCurrency)
    {
        $this->toCurrency = $toCurrency;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getConversionResult()
    {
        return $this->conversionResult;
    }

    /**
     * @param mixed $conversionResult
     */
    public function setConversionResult($conversionResult)
    {
        $this->conversionResult = $conversionResult;
    }
}